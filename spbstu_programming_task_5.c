#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Point.h"
#include "Config.h"
#include "Tail_node.h"
#include "Color_rgb.h"
#include "Config.h"

#define sign(A) (((A) < 0) ? -1 : 1)

static cairo_surface_t *surface = NULL;

static Tail *tail;


#if TAIL_MOUTION_CONTROLL == TAIL_KEYBOARD_CONTROLL
static gboolean on_key_press (GtkWidget *w, GdkEventKey *e, gpointer func_data);
static gboolean on_key_release (GtkWidget *w, GdkEventKey *e);

static gboolean is_up_pressed = FALSE;
static gboolean is_down_pressed = FALSE;
static gboolean is_left_pressed = FALSE;
static gboolean is_right_pressed = FALSE;
#else
static gboolean mouse_motion_event_handler(GtkWidget *widget, GdkEventMotion *event, gpointer data);
static Point *mouse_position;
#endif

static void clear_surface (void);
static gboolean configure_event_cb (GtkWidget *widget, GdkEventConfigure *event, gpointer data);
static gboolean draw_cb (GtkWidget *widget, cairo_t *cr, gpointer data);
static gboolean close_window (gpointer data);
static void activate (GtkApplication *app, gpointer user_data);
static gboolean redraw (gpointer data);
static gboolean update_tail_position (gpointer data);

int main (int argc, char **argv){
    GtkApplication *app;
    int status;

    app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
    g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
    status = g_application_run (G_APPLICATION (app), argc, argv);
    g_object_unref (app);
    destroy_tail(tail->next_node);
    return status;
}


#if TAIL_MOUTION_CONTROLL == TAIL_KEYBOARD_CONTROLL
static gboolean on_key_press (GtkWidget *w, GdkEventKey *e, gpointer func_data){
    switch (e->keyval){
        case GDK_KEY_Up:
            is_up_pressed = TRUE;
            break;
        case GDK_KEY_Down:
            is_down_pressed = TRUE;
            break;
        case GDK_KEY_Left:
            is_left_pressed = TRUE;
            break;
        case GDK_KEY_Right:
            is_right_pressed = TRUE;
            break;
    }
    
    return GDK_EVENT_PROPAGATE;
}

static gboolean on_key_release (GtkWidget *w, GdkEventKey *e){
    switch (e->keyval){
        case GDK_KEY_Up:
            is_up_pressed = FALSE;
            break;
        case GDK_KEY_Down:
            is_down_pressed = FALSE;
            break;
        case GDK_KEY_Left:
            is_left_pressed = FALSE;
            break;
        case GDK_KEY_Right:
            is_right_pressed = FALSE;
            break;
    }
    return GDK_EVENT_PROPAGATE;
}
#endif

static gboolean update_tail_position (gpointer data){
    Point* shift_val = new_point(0,0);
    
#if TAIL_MOUTION_CONTROLL == TAIL_KEYBOARD_CONTROLL
        if (is_up_pressed) shift_val->y -= TAIL_MOVING_DIST;
        if (is_down_pressed) shift_val->y += TAIL_MOVING_DIST;
        if (is_left_pressed) shift_val->x -= TAIL_MOVING_DIST;
        if (is_right_pressed) shift_val->x += TAIL_MOVING_DIST;

#elif TAIL_MOUTION_CONTROLL == TAIL_MOUSE_CONTROLL
        Point *diff_point = new_point(tail->position->x - mouse_position->x, tail->position->y - mouse_position->y);
        DEBUG (printf("Diff(%d, %d)\n", diff_point->x, diff_point->y);)
        if (abs(diff_point->x) > TAIL_MOVING_DIST)
            shift_val->x -= sign(diff_point->x)*TAIL_MOVING_DIST;
        else
            shift_val->x -= diff_point->x;
        
        if (abs(diff_point->y) > TAIL_MOVING_DIST)
            shift_val->y -= sign(diff_point->y)*TAIL_MOVING_DIST;
        else
            shift_val->y -= diff_point->y;
        
        DEBUG(printf("\nShift: %d %d\n", shift_val->x, shift_val->y);)
#endif
        
    int window_width, window_height;
    gtk_window_get_size(GTK_WINDOW(data), &window_width, &window_height);
    if (tail->position->x + shift_val->x > window_width - 20 || tail->position->x + shift_val->x < 0)
        shift_val->x = 0;
    if(tail->position->y + shift_val->y > window_height - 20 || tail->position->y + shift_val->y < 0)
        shift_val->y = 0;
    tail_head_pos_shift(tail, shift_val);
    
    DEBUG(
        printf("\nShift: %d %d\n", shift_val->x, shift_val->y);
        printf("Head: %d %d IS_HEAD:%d Color: (r%f,g%f,b%f)\n", tail->position->x, tail->position->y, tail->is_head, tail->color->red, tail->color->green, tail->color->blue);
        tail = tail->next_node;
        while(!tail->is_head){
        printf("Node: %d %d IS_HEAD:%d Color: (r%f,g%f,b%f)\n", tail->position->x, tail->position->y, tail->is_head, tail->color->red, tail->color->green, tail->color->blue);
        tail = tail->next_node;
        }
    )
    
    destroy_point(shift_val);
    return TRUE;
}

/* Surface to store current scribbles */
static void clear_surface (void){
    cairo_t *cr;
    
    cr = cairo_create (surface);
    cairo_set_source_rgb (cr, 1, 1, 1);
    cairo_paint (cr);
    cairo_destroy (cr);
}

/* Create a new surface of the appropriate size to store our scribbles */
static gboolean configure_event_cb (GtkWidget *widget, GdkEventConfigure *event, gpointer data){
    if (surface)
    cairo_surface_destroy (surface);

    surface = gdk_window_create_similar_surface (gtk_widget_get_window (widget),
                                                CAIRO_CONTENT_COLOR,
                                                gtk_widget_get_allocated_width (widget),
                                                gtk_widget_get_allocated_height (widget));
    /* Initialize the surface to white */
    clear_surface ();

    /* We've handled the configure event, no need for further processing. */
    return TRUE;
}

// Redraw the screen from the surface.
static gboolean draw_cb (GtkWidget *widget, cairo_t *cr, gpointer data){
    cairo_set_source_surface (cr, surface, 0, 0);
    cairo_paint (cr);
    return FALSE;
}

static gboolean redraw (gpointer data){
    cairo_t *cr;
    
    float size;
    float size_shift_val = ((float)TAIL_POINT_SIZE_PX * (float)TAIL_HEAD_END_SIZE_DIFF - (float)TAIL_POINT_SIZE_PX) / TAIL_POINTS_AMOUNT;
    
    if (TAIL_HEAD_MORE_THAN_END) size = TAIL_POINT_SIZE_PX;
    else{
        size = TAIL_POINT_SIZE_PX * TAIL_HEAD_END_SIZE_DIFF;
        size_shift_val = -size_shift_val;
    }

    /* Paint to the surface, where we store our state */
    cr = cairo_create (surface);
    clear_surface();
    
    tail = tail->next_node;
    while (!tail->is_head){
        if (tail->is_visible){
            cairo_set_source_rgb(cr, tail->color->red, tail->color->green, tail->color->blue);
            cairo_arc(cr,tail->position->x, tail->position->y, (int)(size/2), 0, 2*M_PI);
            cairo_fill (cr);
            cairo_stroke(cr);
        }
        size += size_shift_val;
        tail = tail->next_node;
    }
    cairo_set_source_rgb(cr, tail->color->red, tail->color->green, tail->color->blue);
    cairo_arc(cr,tail->position->x, tail->position->y, (int)(size/2), 0, 2*M_PI);
    cairo_fill (cr);
    
    gtk_widget_queue_draw (data);
    cairo_destroy (cr);
    return TRUE;
    /* Now invalidate the affected region of the drawing area. */
}

static gboolean close_window (gpointer data){
    if (surface)
        cairo_surface_destroy (surface);
    return TRUE;
}

#if TAIL_MOUTION_CONTROLL == TAIL_MOUSE_CONTROLL
static gboolean mouse_motion_event_handler(GtkWidget *widget, GdkEventMotion *event, gpointer data){
    /* paranoia check, in case we haven't gotten a configure event */
    if (surface == NULL)
        return FALSE;

    mouse_position->x = (int)event->x;
    mouse_position->y = (int)event->y;

    DEBUG (printf("Mouse event: (%f, %f)\n", event->x, event->y);)
    /* We've handled it, stop processing */
        return TRUE;
    }
#endif

static void activate (GtkApplication *app, gpointer user_data){
    GtkWidget *drawing_area;
    GtkWidget *frame;
    GtkWidget *window;

    Point point = {WINDOW_SIZE_WEIGHT_PX/2, WINDOW_SIZE_HEIGHT_PX/2};
    
    tail = new_tail(&point, TAIL_POINTS_AMOUNT);

#if TAIL_MOUTION_CONTROLL == TAIL_MOUSE_CONTROLL
    mouse_position = new_point(point.x, point.y);
#endif
    
    window = gtk_application_window_new (app);
    gtk_window_set_title (GTK_WINDOW (window), "Drawing Area");

    g_signal_connect (window, "destroy", G_CALLBACK (close_window), NULL);

    gtk_container_set_border_width (GTK_CONTAINER (window), 8);

    frame = gtk_frame_new (NULL);
    gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
    gtk_container_add (GTK_CONTAINER (window), frame);

    drawing_area = gtk_drawing_area_new ();
    /* set a minimum size */
    gtk_widget_set_size_request (drawing_area, WINDOW_SIZE_WEIGHT_PX, WINDOW_SIZE_HEIGHT_PX);

    gtk_container_add (GTK_CONTAINER (frame), drawing_area);

    /* Signals used to handle the backing surface */
    g_signal_connect (drawing_area, "draw",
                    G_CALLBACK (draw_cb), NULL);
    g_signal_connect (drawing_area,"configure-event",
                    G_CALLBACK (configure_event_cb), NULL);
    
        
#if TAIL_MOUTION_CONTROLL == TAIL_KEYBOARD_CONTROLL
        gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
        /* Event signals */
        gtk_event_controller_key_new (window);
        g_signal_connect (G_OBJECT (window), "key_press_event", G_CALLBACK (on_key_press), NULL);
        g_signal_connect (G_OBJECT (window), "key_release_event", G_CALLBACK (on_key_release), NULL);
#else
        gtk_widget_add_events (window, GDK_POINTER_MOTION_MASK);
        g_signal_connect (window, "motion-notify-event",
                    G_CALLBACK (mouse_motion_event_handler), NULL);
#endif
    
    g_timeout_add(DROWING_UPDATE_TIME_MS, G_SOURCE_FUNC(redraw), drawing_area);
    g_timeout_add(TAIL_UPDATE_TIME_MS, G_SOURCE_FUNC(update_tail_position), window);
    /* Ask to receive events the drawing area doesn't normally
    * subscribe to. In particular, we need to ask for the
    * button press and motion notify events that want to handle.
    */
    gtk_widget_set_events (drawing_area, gtk_widget_get_events (drawing_area)
                                        | GDK_BUTTON_PRESS_MASK
                                        | GDK_POINTER_MOTION_MASK);

    gtk_widget_show_all (window);
}
