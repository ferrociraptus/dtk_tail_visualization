#ifndef TAIL_NODE_H
#define TAIL_NODE_H

#include "Color_rgb.h"
#include "Point.h"

typedef struct TailNode{
    struct TailNode *next_node;
    Point *position;
    ColorRGB *color;
    unsigned char is_head :1;
    unsigned char is_visible :1;
} Tail;

Tail *new_tail_node(Point* position, ColorRGB *color);
Tail *new_tail(Point* start_pos, int nodes_amount);
void tail_head_pos_shift(Tail *node, Point *point);
void go_to_next_node(Tail *node);
void destroy_tail(Tail *head);

#endif
