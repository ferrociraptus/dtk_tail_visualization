#include "Color_rgb.h"
#include <stdlib.h>

ColorRGB *copy_color(ColorRGB *color){
    ColorRGB *new_c = new_color(color->red, color->green, color->blue);
    return new_c;
};

ColorRGB *new_color(float red, float green, float blue){
    ColorRGB *color = ((ColorRGB*) malloc(sizeof(ColorRGB)));
    color->red = red;
    color->green = green;
    color->blue = blue;
    return color;
};

void destroy_color(ColorRGB* color){
    free(color);
};

float in_color_range(float value){
    if (value < 0) return 0.0;
    if (value > 1) return 1.0;
    return value;
};

void increase_color(ColorRGB* color, float red, float green, float blue){
    color->red = in_color_range(color->red + red);
    color->green = in_color_range(color->green + green);
    color->blue = in_color_range(color->blue + blue);
};

void increase_all_color(ColorRGB* color, float val){
    increase_color(color,val,val,val);
};

void add_to_color_color(ColorRGB* to, ColorRGB* from){
    increase_color(to, from->red, from->green, from->blue);
}

char is_white(ColorRGB *color){
    return (color->red == 1.0 && color->green == 1.0 && color->blue == 1.0) ? 1 : -1;
};

char is_black(ColorRGB *color){
    return (color->red == 0.0 && color->green == 0.0 && color->blue == 0.0) ? 1 : -1;
};

void make_white(ColorRGB *color){
    color->red = 1;
    color->green = 1;
    color->blue = 1;
};

void make_black(ColorRGB *color){
    color->red = 0;
    color->green = 0;
    color->blue = 0;
};
