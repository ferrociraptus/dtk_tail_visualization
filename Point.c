#include "Point.h"
#include <stdlib.h>


Point *new_point(signed int x, signed int y){
    Point* point = (Point*) malloc(sizeof(Point));
    point->x = x;
    point->y = y;
    return point;
};

Point *copy_point(Point *point){
    return new_point(point->x, point->y);
};

void copy_point_val(Point *from, Point *to){
    to->x = from->x;
    to->y = from->y;
};

void destroy_point(Point* point){
    free(point);
};

Point *point_summ(Point* point1, Point *point2){
    return new_point(point1->x + point2->x, point1->y + point2->y);
};

void add_to_point_value(Point *point, signed int x, signed int y){
    point->x += x;
    point->y += y;
};

void add_to_point_point(Point *point, Point *point2){
    point->x += point2->x;
    point->y += point2->y;
};

char is_point_equal(Point *point, Point *point2){
    if (point->x == point2->x && point->y == point2->y) return 1;
    else return 0;
};
