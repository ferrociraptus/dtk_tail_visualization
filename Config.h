#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include "Color_rgb.h"

//=======Updating time settings========
#define DROWING_UPDATE_TIME_MS 10
#define TAIL_UPDATE_TIME_MS 20 //Moving speed
//=====================================


//=========Tail view settings==========
#define TAIL_POINT_SIZE_PX 6 //MIN point value, MAX point value=TAIL_POINT_SIZE_PX*TAIL_HEAD_END_SIZE_DIFF
#define TAIL_POINTS_AMOUNT 150
#define TAIL_MOVING_DIST TAIL_POINT_SIZE_PX / 2 // distance between two points in tail
#define TAIL_HEAD_END_SIZE_DIFF 4 // deifference between head point size and end point size
#define TAIL_HEAD_MORE_THAN_END 1
//=====================================


//============Color settings============
// --------- Colors ---------
#define TAIL_BLACK 1
#define TAIL_FIRE 2
#define TAIL_RED 3
#define TAIL_GREEN 4
#define TAIL_BLUE 5
// --------------------------


#define TAIL_HEAD_COLOR TAIL_BLACK
//======================================



//====Tail moution controll settings====
//------Controll types-------
#define TAIL_KEYBOARD_CONTROLL 1
#define TAIL_MOUSE_CONTROLL 2
//---------------------------

#define TAIL_MOUTION_CONTROLL TAIL_KEYBOARD_CONTROLL
//======================================


//===========Window settings============
#define WINDOW_SIZE_HEIGHT_PX 400
#define WINDOW_SIZE_WEIGHT_PX 600
//======================================


//===========Debug settings=============
#define DEBUG_OUTPUT 0


#define DEBUG(A) if(DEBUG_OUTPUT){A} 
//======================================
#endif
