#ifndef MY_POINT_H
#define MY_POINT_H

typedef struct _Point{
    signed int x;
    signed int y;
} Point;

Point *new_point(signed int x, signed int y);
Point *copy_point(Point *point);
void copy_point_val(Point *from, Point *to);
void destroy_point(Point *point);
Point *point_summ(Point *point1, Point *point2);

void add_to_point_value(Point *point, signed int x, signed int y);
void add_to_point_point(Point *point, Point *point2);

char is_point_equal(Point *point, Point *point2);

#endif
