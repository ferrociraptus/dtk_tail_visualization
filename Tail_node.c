#include "Tail_node.h"
#include <stdlib.h>
#include "Point.h"
#include "Config.h"

// static int viewing_nodes;

Tail *new_tail_node(Point* position, ColorRGB *color){
    Tail *new_tail = (Tail*) malloc(sizeof(Tail));
    new_tail->position = copy_point(position);
    new_tail->color = copy_color(color);
    new_tail->is_visible = 0;
    new_tail->is_head = 0;
    return new_tail;
};

Tail *new_tail(Point* start_pos, int nodes_amount){
    ColorRGB *tail_head_color;
    ColorRGB *buff_color = new_color(1.0, 1.0, 1.0);
    ColorRGB *color_shift_value;
    float reduce_color_val = -(1.0 / nodes_amount);
    Tail *node;

    #if TAIL_HEAD_COLOR == TAIL_BLACK
            color_shift_value = new_color(reduce_color_val, reduce_color_val, reduce_color_val);
            tail_head_color = new_color(0, 0, 0);

    #elif TAIL_HEAD_COLOR == TAIL_FIRE
            buff_color = new_color(0.9, 1.0, 0.0);
            color_shift_value = new_color(0, reduce_color_val, 0);
            tail_head_color = new_color(0.9, 0, 0);
            
    #elif TAIL_HEAD_COLOR == TAIL_RED
            color_shift_value = new_color(0, reduce_color_val, reduce_color_val);
            tail_head_color = new_color(1.0, 0, 0);
            
    #elif TAIL_HEAD_COLOR == TAIL_GREEN
            color_shift_value = new_color(reduce_color_val, 0, reduce_color_val);
            tail_head_color = new_color(0, 1.0, 0);
        
    #elif TAIL_HEAD_COLOR == TAIL_BLUE
            color_shift_value = new_color(reduce_color_val, reduce_color_val, 0);
            tail_head_color = new_color(0, 0, 1.0);
    #endif
    
    node = new_tail_node(start_pos, tail_head_color);
    node->is_head = 1;
    node->is_visible = 1;
    Tail *start_node = node;
    for (int i = 0; i < nodes_amount - 1; i++){
        add_to_color_color(buff_color, color_shift_value);
        node->next_node = new_tail_node(start_pos, buff_color);
        node = node->next_node;
    }
    node->next_node = start_node;
    node = node->next_node;
    
    destroy_color(buff_color);
    destroy_color(color_shift_value);
    destroy_color(tail_head_color);
    
    return start_node;
};

void tail_head_pos_shift(Tail *node, Point *point){
    node = node->next_node;
    while (node->next_node->position == NULL) node = node->next_node;
    node->position = copy_point(node->next_node->position);
    node = node->next_node;
    
    while (!node->is_head){
        if (is_point_equal(node->next_node->position, node->position)) node->is_visible = 0;
        else node->is_visible = 1;
//         node->is_visible = 1;
        copy_point_val(node->next_node->position, node->position);
        node = node->next_node;
    }
    
    add_to_point_point(node->position, point);
};

void destroy_tail(Tail *node){
    if (!node->is_head)
        destroy_tail(node->next_node);
    destroy_color(node->color);
    destroy_point(node->position);
    free(node);
};
