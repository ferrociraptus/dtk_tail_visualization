all: spbstu_programming_task_5

WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O2
CC = clang

spbstu_programming_task_5: spbstu_programming_task_5.c point.o color_rgb.o tail.o Config.h
	$(CC) `pkg-config --cflags --libs gtk+-3.0` -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) spbstu_programming_task_5.c *.o
	
point.o: Point.c
	$(CC) $(WARNINGS) $(DEBUG) $(OPTIMIZE) -c -o $@ Point.c
	
color_rgb.o: Color_rgb.c
	$(CC) $(WARNINGS) $(DEBUG) $(OPTIMIZE) -c -o $@ Color_rgb.c
	
tail.o: Tail_node.c color_rgb.o point.o Config.h
	$(CC) $(WARNINGS) $(DEBUG) $(OPTIMIZE) -c -o $@ Tail_node.c
	
clean:
	rm -f spbstu_programming_task_5
	rm *.o

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./spbstu_programming_task_5

